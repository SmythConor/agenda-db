CREATE DATABASE IF NOT EXISTS agenda;
GRANT ALL PRIVILEGES ON `agenda`.* TO 'mt_user'@'localhost' IDENTIFIED BY '6a3e9ndIhMrbMoPCZaFBJO6B';
FLUSH PRIVILEGES;

CONNECT agenda;

source ../ddl/create_table_verified.sql
source ../ddl/create_table_priority.sql
source ../ddl/create_table_users.sql
source ../ddl/create_table_group_parent.sql
source ../ddl/create_table_group_users.sql
source ../ddl/create_seq_group_users_key_id.sql
source ../ddl/create_table_shared_calendar.sql
source ../ddl/create_table_to_do_list.sql
source ../ddl/create_table_messages.sql

source ../dml/insert_into_verified.sql
source ../dml/insert_into_priority.sql
source ../dml/insert_into_seq_grp_usrs_key_id.sql

source ../package/get_user_by_email.sql
source ../package/create_user.sql

source ../package/create_group.sql
source ../package/get_groups_for_user.sql
source ../package/add_group_member.sql
source ../package/update_name_group.sql
source ../package/remove_group_member.sql

source ../package/get_entries_calendar.sql
source ../package/create_entry_calendar.sql
source ../package/update_entry_calendar.sql
source ../package/delete_entry_calendar.sql
source ../package/get_entries_calendar_by_id.sql

source ../package/get_entries_todo_list.sql
source ../package/create_entry_todo_list.sql
source ../package/update_entry_todo_list.sql
source ../package/delete_entry_todo_list.sql
source ../package/get_entries_todo_list_by_id.sql

source ../package/get_messages.sql
source ../package/create_message.sql
source ../package/update_message.sql

source ../dml/seed_users.sql
source ../dml/seed_groups.sql
source ../dml/seed_calendar_entries.sql
source ../dml/seed_todo_list_entries.sql

source ../grants/grant_users.sql
source ../grants/grant_create_group.sql
source ../grants/grant_get_group_for_user.sql
source ../grants/grant_add_group_member.sql

FLUSH PRIVILEGES;
