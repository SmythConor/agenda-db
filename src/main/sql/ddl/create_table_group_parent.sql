CREATE TABLE IF NOT EXISTS group_parent
	(
		group_id INTEGER NOT NULL AUTO_INCREMENT,
		group_admin_id INTEGER NOT NULL,
		group_users_key_id INTEGER NOT NULL,
		name VARCHAR(100) NOT NULL,
		notification_key VARCHAR(255) NOT NULL,
		notification_key_name VARCHAR(255) NOT NULL,
		PRIMARY KEY (group_id),
		FOREIGN KEY (group_admin_id) REFERENCES users(user_id)
	);
