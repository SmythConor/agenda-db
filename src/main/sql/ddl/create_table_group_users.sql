CREATE TABLE IF NOT EXISTS agenda.group_users
	(
		group_users_key_id INTEGER NOT NULL,
		user_id INTEGER NOT NULL,
		group_id INTEGER NOT NULL,
		PRIMARY KEY (group_users_key_id),
		FOREIGN KEY (user_id) REFERENCES users(user_id),
		FOREIGN KEY (group_id) REFERENCES group_parent(group_id)
	);
