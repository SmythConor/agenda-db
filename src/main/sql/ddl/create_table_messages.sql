CREATE TABLE IF NOT EXISTS messages
 (
   message_id INTEGER NOT NULL,
	 message_body VARCHAR(255) NOT NULL,
	 sender_id INTEGER NOT NULL,
	 group_id INTEGER NOT NULL,
	 sent_tmstmp TIMESTAMP NOT NULL,
	 delivered_tmstmp TIMESTAMP NULL,
	 PRIMARY KEY (message_id),
	 FOREIGN KEY (sender_id) REFERENCES users(user_id),
	 FOREIGN KEY (group_id) REFERENCES group_parent(group_id)
 );
