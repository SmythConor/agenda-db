CREATE TABLE IF NOT EXISTS priority
	(
		priority VARCHAR(10) NOT NULL,
		PRIMARY KEY (priority)
	)
