CREATE TABLE IF NOT EXISTS agenda.shared_calendar
	(
		entry_id INTEGER NOT NULL AUTO_INCREMENT,
		group_id INTEGER NOT NULL,
		user_id INTEGER NOT NULL,
		title VARCHAR(100) NOT NULL,
		description VARCHAR(100),
		recur_count INTEGER NOT NULL,
		entry_tmstmp TIMESTAMP NOT NULL,
		reminder_tmstmp TIMESTAMP NULL,
		insert_tmstmp TIMESTAMP NOT NULL,
		update_tmstmp TIMESTAMP NOT NULL,
		last_update_id INTEGER,
		update_comment MEDIUMTEXT,
		PRIMARY KEY (entry_id),
		FOREIGN KEY (group_id) REFERENCES group_parent(group_id),
		FOREIGN KEY (user_id) REFERENCES users(user_id)
	);
