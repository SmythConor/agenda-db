CREATE TABLE IF NOT EXISTS agenda.to_do_list
	(
		entry_id INTEGER NOT NULL AUTO_INCREMENT,
		group_id INTEGER NOT NULL,
		user_id INTEGER NOT NULL,
		title VARCHAR(100) NOT NULL,
		description VARCHAR(255),
		priority VARCHAR(10) NOT NULL,
		reminder_tmstmp TIMESTAMP NULL,
		insert_tmstmp TIMESTAMP,
		update_tmstmp TIMESTAMP,
		last_update_id INTEGER,
		update_comment VARCHAR(255),
		PRIMARY KEY (entry_id),
		FOREIGN KEY (priority) REFERENCES priority(priority),
		FOREIGN KEY (group_id) REFERENCES group_parent(group_id),
		FOREIGN KEY (user_id) REFERENCES users(user_id)
	);
