CREATE TABLE IF NOT EXISTS agenda.users
  (
     user_id INTEGER NOT NULL AUTO_INCREMENT,
     name   VARCHAR(100) NOT NULL,
     email  VARCHAR(100) NOT NULL UNIQUE,
     password VARCHAR(255) NOT NULL,
		 salt VARCHAR(255) NOT NULL,
		 verified VARCHAR(255) NOT NULL,
		 registration_id VARCHAR(255) NOT NULL,
		 PRIMARY KEY (user_id),
		 FOREIGN KEY (verified) REFERENCES verified(is_verified)
  ); 
