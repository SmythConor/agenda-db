DELIMITER //
CREATE PROCEDURE
  add_group_member
                    (
                    IN i_group_id INTEGER,
                    IN i_user_id  INTEGER
                    )
BEGIN
  UPDATE group_users_key_id_seq
  SET    id=last_insert_id(id + 1);

  INSERT INTO group_users
              (
                          group_users_key_id,
                          user_id,
													group_id
              )
  SELECT LAST_INSERT_ID(),
         i_user_id,
				 group_id
  FROM   group_parent
  WHERE  group_id=i_group_id;
END//
DELIMITER ;
