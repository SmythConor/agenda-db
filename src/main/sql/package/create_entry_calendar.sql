DELIMITER //
CREATE PROCEDURE create_entry_calendar (
																					IN i_group_id INTEGER,
																					IN i_user_id INTEGER,
																					IN i_title VARCHAR(100), 
																					IN i_description VARCHAR(100),
																					IN i_recur_count INTEGER,
																					IN i_entry_tmstmp TIMESTAMP,
																					IN i_reminder_tmstmp TIMESTAMP
																				)
BEGIN
INSERT INTO shared_calendar
              ( 
                          group_id, 
													user_id,
                          title, 
                          description, 
													recur_count,
													entry_tmstmp,
													reminder_tmstmp,
                          insert_tmstmp,
                          update_tmstmp 
              ) 
              VALUES 
              ( 
                          i_group_id, 
													i_user_id,
                          i_title, 
                          i_description,
													i_recur_count,
													i_entry_tmstmp,
                          i_reminder_tmstmp, 
                          now(), 
                          now() 
              );
END//
DELIMITER ;
