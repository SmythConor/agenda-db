DELIMITER //
CREATE PROCEDURE create_entry_todo_list (
																					 IN i_group_id INTEGER,
																					 IN i_user_id INTEGER,
																					 IN i_title VARCHAR(100),
																					 IN i_description VARCHAR(255),
																					 IN i_priority VARCHAR(10),
																					 IN i_reminder_tmstmp TIMESTAMP
																				)
BEGIN
INSERT INTO to_do_list
							(
												group_id,
												user_id,
												title,
												description,
												priority,
												reminder_tmstmp,
												insert_tmstmp,
												update_tmstmp
							)
							VALUES
							(
												i_group_id,
												i_user_id,
												i_title,
												i_description,
												i_priority,
												i_reminder_tmstmp,
												now(),
												now()
							);
END//
DELIMITER ;
