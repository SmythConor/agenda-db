DELIMITER //
CREATE PROCEDURE
  create_group(IN i_group_admin_id INTEGER,
               IN i_name           VARCHAR(255),
							 IN i_notification_key VARCHAR(255),
							 IN i_notification_key_name VARCHAR(255))
BEGIN
  UPDATE group_users_key_id_seq
  SET    id=last_insert_id(id + 1);
	
	INSERT INTO group_parent
              (
                          group_users_key_id,
                          group_admin_id,
                          name,
													notification_key,
													notification_key_name
              )
  SELECT id,
         i_group_admin_id,
         i_name,
				 i_notification_key,
				 i_notification_key_name
  FROM   group_users_key_id_seq;
	
	INSERT INTO group_users
              (
                          group_users_key_id,
                          user_id,
													group_id
              )
  SELECT group_users_key_id,
         i_group_admin_id,
				 group_id
  FROM   group_parent
  WHERE  group_id=Last_insert_id();
END//
DELIMITER ;
