DELIMITER //
CREATE PROCEDURE create_message (
                                  IN i_message_id INTEGER,
																	IN i_message_body VARCHAR(255),
																	IN i_sender_id INTEGER,
																	IN i_group_id INTEGER,
																	IN i_sent_tmstmp TIMESTAMP
                                )
BEGIN
INSERT INTO messages
              (
								message_id,
								message_body,
								sender_id,
								group_id,
								sent_tmstmp
							)
							VALUES
							(
								i_message_id,
								i_message_body,
								i_sender_id,
								i_group_id,
								i_sent_tmstmp
							);
END//
DELIMITER ;
