DELIMITER //
CREATE PROCEDURE
  create_user
               (
               IN i_name     VARCHAR(255),
               IN i_email    VARCHAR(255),
               IN i_password VARCHAR(255),
               IN i_salt     VARCHAR(255),
							 IN i_registration_id VARCHAR(255)
               )
begin
  INSERT INTO users
              (
                          name,
                          email,
                          password,
                          salt,
                          verified,
													registration_id
              )
              VALUES
              (
                          i_name,
                          i_email,
                          i_password,
                          i_salt,
                          'NOT',
													i_registration_id
              );
end//
delimiter ;
