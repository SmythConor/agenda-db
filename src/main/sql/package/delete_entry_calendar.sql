DELIMITER //
CREATE PROCEDURE delete_entry_calendar (
																					IN i_entry_id INTEGER
																			 )
BEGIN
	DELETE FROM shared_calendar
	WHERE entry_id=i_entry_id;
END//
DELIMITER ;
