DELIMITER //
CREATE PROCEDURE delete_entry_todo_list (
																					IN i_entry_id INTEGER
																			 )
BEGIN
	DELETE FROM to_do_list
	WHERE entry_id=i_entry_id;
END//
DELIMITER ;
