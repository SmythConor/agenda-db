DELIMITER //
CREATE PROCEDURE get_entries_calendar ( in i_group_id integer ) 
BEGIN
SELECT entry_id,
        group_id, 
				user_id,
        title, 
        description,
				recur_count,
				entry_tmstmp,
        reminder_tmstmp, 
        insert_tmstmp, 
        update_tmstmp,
				last_update_id,
				update_comment
  FROM   shared_calendar 
  WHERE  group_id = i_group_id;
END//
