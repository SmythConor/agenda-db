DELIMITER //
CREATE PROCEDURE get_entries_calendar_by_id (
																							IN i_entry_id INTEGER
																						)
BEGIN
SELECT entry_id,
			 group_id,
			 user_id,
			 title,
			 description,
			 recur_count,
			 entry_tmstmp,
			 reminder_tmstmp,
			 insert_tmstmp,
			 update_tmstmp,
			 last_update_id,
			 update_comment
FROM shared_calendar
WHERE entry_id = i_entry_id;
END//
DELIMITER ;
