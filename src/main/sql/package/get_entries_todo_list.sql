DELIMITER //
CREATE PROCEDURE get_entries_todo_list (
																					IN i_group_id INTEGER
																			 )
BEGIN
SELECT entry_id,
			 group_id,
			 user_id,
			 title,
			 description,
			 priority,
			 reminder_tmstmp,
			 insert_tmstmp,
			 update_tmstmp,
			 last_update_id,
			 update_comment
FROM to_do_list
WHERE group_id = i_group_id;
END//
DELIMITER ;
