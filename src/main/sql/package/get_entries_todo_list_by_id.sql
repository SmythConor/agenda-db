DELIMITER //
CREATE PROCEDURE get_entries_todo_list_by_id (
                                              IN i_entry_id INTEGER
																						 )
BEGIN
SELECT entry_id,
			 group_id,
			 user_id,
			 title,
			 description,
			 priority,
			 reminder_tmstmp,
			 insert_tmstmp,
			 update_tmstmp,
			 last_update_id,
			 update_comment
FROM to_do_list
WHERE entry_id = i_entry_id;
END//
DELIMITER ;
