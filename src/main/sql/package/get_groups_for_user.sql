DELIMITER //
CREATE PROCEDURE
  get_groups_for_user
                      (
                      IN i_user_id INTEGER
                      )
begin
  SELECT usrs.group_id,
				 prt.group_admin_id,
				 name,
				 prt.notification_key,
				 prt.notification_key_name
  FROM   group_users usrs
  JOIN  (group_parent prt)
  ON     usrs.group_id=prt.group_id
  WHERE  usrs.user_id=i_user_id;
END//
