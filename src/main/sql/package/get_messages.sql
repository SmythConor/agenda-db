DELIMITER //
CREATE PROCEDURE get_messages (
                                IN i_group_id INTEGER
							                )
BEGIN
SELECT message_id,
       sender_id,
			 group_id,
			 message_body,
			 sent_tmstmp,
			 delivered_tmstmp
FROM messages
WHERE group_id=i_group_id;
END//
DELIMITER ;
