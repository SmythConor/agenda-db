DELIMITER //
CREATE PROCEDURE
  get_user_by_email(IN i_email VARCHAR(255))
begin
  SELECT user_id,
         name,
         email,
				 password,
				 salt,
         verified,
				 registration_id
  FROM   users
  WHERE  email=i_email;
END//
delimiter ;
