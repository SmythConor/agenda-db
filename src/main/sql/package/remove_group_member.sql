DELIMITER //
CREATE PROCEDURE
	remove_group_member
											(
											IN i_group_id INTEGER,
											IN i_user_id INTEGER 
											)
BEGIN
	DELETE FROM group_users
				 WHERE 	group_id = i_group_id AND user_id = i_user_id;
END//
DELIMITER ;
