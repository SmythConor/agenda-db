DELIMITER //
CREATE PROCEDURE update_entry_calendar (
                                          IN i_entry_id INTEGER,
																					IN i_user_id INTEGER,
																					IN i_title VARCHAR(100), 
																					IN i_description VARCHAR(100), 
																					IN i_recur_count INTEGER, 
																					IN i_entry_tmstmp TIMESTAMP, 
																					IN i_reminder_tmstmp TIMESTAMP, 
																					IN i_update_comment VARCHAR(255)
																				)
BEGIN
UPDATE shared_calendar
SET title = i_title,
    description = i_description,
		recur_count = i_recur_count,
		entry_tmstmp = i_entry_tmstmp,
		reminder_tmstmp = i_reminder_tmstmp,
		update_tmstmp = now(),
		last_update_id = i_user_id,
		update_comment = i_update_comment
WHERE entry_id=i_entry_id;
END//
