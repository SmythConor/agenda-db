DELIMITER //
CREATE PROCEDURE update_message (
                                  IN i_message_id INTEGER,
                                  IN i_delivered_tmstmp TIMESTAMP
                                )
BEGIN
UPDATE messages
SET delivered_tmstmp = i_delivered_tmstmp
WHERE message_id = i_message_id;
END//
DELIMITER ;
