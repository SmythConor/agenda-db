DELIMITER //
CREATE PROCEDURE
	update_name_group
										(
										IN i_group_id INTEGER,
										IN i_name VARCHAR(100)
										)
BEGIN
	UPDATE group_parent 
	SET name = i_name
	WHERE group_id = i_group_id;
END//
DELIMITER ;
